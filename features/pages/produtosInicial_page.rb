class ProdutosInicial < SitePrism::Page
    include Capybara::DSL

    #Produtos Comuns
    element :produto_com_estoque, "a[href='/item/423404/produto-com-estoque']"
    element :produto_sem_controle_estoque, "a[href='/item/423409/produto-sem-controle-de-estoque']"

    #Produtos com modificadores
    element :produto_com_2_modificadores, "a[href='/item/423417/produto-com-2-modificadores']"
    element :produto_com_2_modificadores_obrigatorios, "a[href='/item/423415/produto-com-2-modificadores-obrigatorios']"
    element :produto_com_1_modificador, "a[href='/item/423418/produto-com-1-modificador']"
    element :produto_com_1_modificador_obrigatorio, "a[href='/item/423416/produto-com-1-modificador-obrigatorio']"

    #Check do modificador
    elements :escolher_modificador, "label[class='sc-hzMMCg kefMIQ']"

    #Produtos com variações e modificadores
    elements :produto_com_variacao, "a[href='/item/423405/produto-com-variacao']"
    elements :produto_com_variacao_1modificador, "a[href='/item/423414/produto-com-variacao-e-1-modificador']"
    elements :produto_com_variacao_1modificador_obrigatorio, "a[href='/item/423412/produto-com-variacao-e-1-modificador-obrigatorio']"
    elements :produto_com_variacao_2modificadores, "a[href='/item/423413/produto-com-variacao-e-2-modificadores']"
    elements :produto_com_variacao_2modificadores_obrigatorios, "a[href='/item/423406/produto-com-variacao-e-2-modificadores-obrigatorios']"
    elements :produto_com_variacao_sem_controle_estoque, "a[href='/item/423411/produto-com-variacao-sem-controle-de-estoque-na-variacao']"

    #Variações dentro do produto
    element :abrir_variacao, "input[autocapitalize='none']"
    elements :variacao_1, :xpath, "//div[text()='Teste 1']"
    element :variacao_2, :xpath, "//div[text()='Teste 2']"

    #Categorias
    element :categoria_produtos_comum, "a[title='Produto Comum']"
    element :categoria_produtos_com_modificadores, "a[title='Produtos com Modificadores']"
    element :categoria_produtos_com_variacoes, "a[title='Produtos com Variações']"
    
    #Elementos adicionais
    element :adicionar, :xpath, "//div[text()='ADICIONAR']"
    element :adicionar_mais_produtos, :xpath, "//div[text()='ADICIONAR MAIS PRODUTOS']"
    element :finalizar_compra, :xpath, "//span[text()='FINALIZAR COMPRA']"
    element :carrinho, "i[class='sc-emTkGv cztTMG fa fa-shopping-cart']"
    
    def fecharCarrinho
        carrinho.click
        finalizar_compra.click
        sleep 3
    end

    def adicionarCarrinho
        adicionar.click
        adicionar_mais_produtos.click
        sleep 2
    end

    def escolherVariacao1
        sleep 1
        abrir_variacao.click
        variacao_1[0].click
    end

    def escolher2Modificadores
        escolher_modificador[1].click
        escolher_modificador[0].click
    end

    def escolher3Modificadores
        escolher_modificador[2].click
        escolher_modificador[1].click
        escolher_modificador[0].click
    end

    def escolher4Modificadores
        escolher_modificador[3].click
        escolher_modificador[2].click
        escolher_modificador[1].click
        escolher_modificador[0].click
    end

    def adicionaProdutos
        categoria_produtos_comum.click
        produto_com_estoque.click
        adicionarCarrinho
        produto_sem_controle_estoque.click
        adicionarCarrinho
        categoria_produtos_com_modificadores.click
        produto_com_1_modificador_obrigatorio.click
        escolher2Modificadores
        adicionarCarrinho
        produto_com_2_modificadores.click
        escolher3Modificadores
        adicionarCarrinho
        produto_com_2_modificadores_obrigatorios.click
        escolher4Modificadores
        adicionarCarrinho
        produto_com_1_modificador.click
        escolher2Modificadores
        adicionarCarrinho
        categoria_produtos_com_variacoes.click
        produto_com_variacao[0].click
        escolherVariacao1
        adicionarCarrinho
        produto_com_variacao_1modificador[0].click
        escolherVariacao1
        escolher2Modificadores
        adicionarCarrinho
        produto_com_variacao_1modificador_obrigatorio[0].click
        escolherVariacao1
        escolher2Modificadores
        adicionarCarrinho
        produto_com_variacao_2modificadores[0].click
        escolherVariacao1
        escolher3Modificadores
        adicionarCarrinho
        produto_com_variacao_2modificadores_obrigatorios[0].click
        escolherVariacao1
        escolher4Modificadores
        adicionarCarrinho
        produto_com_variacao_sem_controle_estoque[0].click
        escolherVariacao1
        adicionarCarrinho
    end

end