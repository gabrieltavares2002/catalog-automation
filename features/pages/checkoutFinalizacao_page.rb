class CheckoutFinalizacao < SitePrism::Page
    include Capybara::DSL

    elements :observacao_produto, :xpath, "//p[text()='Adicionar observação']"
    element :texto_observacao_produto, "textarea[label='Observação']"
    
    element :retirada, "label[for='delivery-radioRetirar no localPICKUP']"

    element :calcular_frete, "label[for='delivery-radioCalcular freteDELIVERY']"
    element :cep, "label[for='cepCEPcep']"
    element :logradouro, "label[for='logradouroLogradourologradouro']"
    element :digitar_cep, "input[id='cep']"
    element :calcular, :xpath, "//button[text()='Calcular']"

    element :cupom_desconto, "input[id='coupon']"
    element :aplicar_cupom, :xpath, "//button[text()='Aplicar']"

    elements :estado_cidade, "div[class='react-select__control css-yk16xz-control']"
    elements :estado_cidade1, "div[class='react-select__value-container react-select__value-container--has-value css-1hwfws3']"
    element :estado_opcoes, :xpath, "//div[text()='SP']"
    element :cidade_opcoes, :xpath, "//div[text()='Santo André']"
    element :rua_logradouro, "input[id='logradouro']"
    element :numero_logradouro, "input[id='numero']"
    element :complemento, "input[id='complemento']"

    element :pessoa_fisica, "input[value='FISICA']"
    element :nome, "input[id='name']"
    element :pessoa_juridica, "input[value='JURIDICA']"
    element :razao_social,"input[id='razaoSocial']"
    element :cpf_cnpj,"input[id='documento']"
    element :fantasia, "input[id='fantasia']"
    element :email, "input[id='email']"
    element :telefone, "input[id='foneFormatted']"

    element :proximo, :xpath, "//button[text()='Próximo']"

    #Pagamento Online com cartão
    element :bandeiras, "div[class='sc-hKgILt beOqPw col mb-2 col-12']"
    element :pagamento_online, "label[for='paymentTypePague on-line com cartão de créditogatewayPagseguro']"
    element :nome_titular, "input[id='nameHolder']"
    element :cpf_titular, "input[id='cpfHolder']"
    element :nascimento_titular, "input[id='birthDateHolder']"
    element :numero_cartao, "input[id='cardNumber_unformatted']"
    element :validade_cartao, "input[id='expiration']"
    element :cod_cvv_cartao, "input[id='cvv']"
    element :parcelas_cartao, :xpath, "//div[text()='Preencha as informações do cartão para ver as opções de parcelamento']"
    element :parcela1, :xpath, "//div[text()='1 parcela']"

    #Tipos de pagamentos presenciais 
    element :pagamento_presencial, "label[for='paymentTypePague na entrega ou retiradaofflinePayment']"
    element :tipo_pagamento_presencial, "div[class='react-select__control css-yk16xz-control']"
    element :opcao_tipo_pagamento_dinheiro, :xpath, "//div[text()='Dinheiro']"
    element :opcao_tipo_pagamento_cheque, :xpath, "//div[text()='Cheque']"
    element :opcao_tipo_pagamento_debito, :xpath, "//div[text()='Débito']"
    element :opcao_tipo_pagamento_credito, :xpath, "//div[text()='Crédito']"
    element :opcao_tipo_pagamento_credito_Parcelado, :xpath, "//div[text()='Crédito Parcelado']"
    element :opcao_tipo_pagamento_credito_parc_comprador, :xpath, "//div[text()='Crédito Parc. Comprador']"
    element :opcao_tipo_pagamento_alimentacao_refeicao, :xpath, "//div[text()='Alimentação/Refeição']"
    element :opcao_tipo_pagamento_fiado, :xpath, "//div[text()='Fiado']"
    element :opcao_tipo_pagamento_qr_code_and_pix, :xpath, "//div[text()='QR Code/PIX']"
    element :opcao_tipo_pagamento_boleto, :xpath, "//div[text()='Boleto']"
    element :opcao_tipo_pagamento_venda_digitada, :xpath, "//div[text()='Venda Digitada']"


    element :recaptcha, "div[class='sc-hKgILt beOqPw col d-flex justify-content-end mb-3 col-12 col-sm-8 col-md-6 col-lg-6 col-xl-6']"
    element :troco_cinquenta_mil, "input[name='valorRecebido']"
    element :observacao_pedido, "textarea[id='observacao']"
    elements :botao_finalizar, "button[type='submit']"

    def avançarStep
        proximo.click    
    end

    def adicionarObservacao 
        observacao_produto[0].click
        texto_observacao_produto.set "Teste Observação"
    end

    def calcularCep
        calcular_frete.click
        cep.click
        digitar_cep.set "09060320"
        calcular.click
        sleep 2
    end
    
    def cupomPorcentagem
        cupom_desconto.set "50 porcento"
        aplicar_cupom.click
        avançarStep
    end

    def cupomValor
        cupom_desconto.set "500 conto"
        aplicar_cupom.click
        avançarStep
    end

    def pessoaFisica
        pessoa_fisica.click
        nome.set "Física SmartPOS"
        cpf_cnpj.set "49395791829"
        email.set "smartpos@teste.com"
        telefone.set "11 99999-9999"
        avançarStep
    end

    def pessoaJuridica
        pessoa_juridica.click
        razao_social.set "Jurídica SmartPOS"
        fantasia.set "Teste Jurídica"
        cpf_cnpj.set "65019499000124"
        email.set "smartpos@teste.com"
        telefone.set "11 99999-9999"
        avançarStep
    end

    def preencherEnderecoCep
        sleep 2
        numero_logradouro.set "123"
        complemento.set "Teste Complemento"
        sleep 2
        avançarStep
    end

    def preencherPagamentoOnline
        pagamento_online.click
        wait_until_bandeiras_visible
        nome_titular.set "Teste Compra Online"
        cpf_titular.set "49395791829"
        nascimento_titular.set "10082002"
        numero_cartao.set "4242424242424242"
        validade_cartao.set "12/2023"
        cod_cvv_cartao.set "123"
        sleep 1
    end

    def pagamentoDinheiro
        pagamento_presencial.click
        tipo_pagamento_presencial.click
        opcao_tipo_pagamento_dinheiro.click
    end

    def troco
        troco_cinquenta_mil.set "50000"
    end

    def observacaoPedido
        observacao_pedido.set "Teste observação pedido"
    end

    def calcularLogradouro
        calcular_frete.click
        logradouro.click
        sleep 1
        estado_cidade[0].set "SP"
        estado_opcoes.click
        sleep 1
        estado_cidade1[1].set "santo andré"
        cidade_opcoes.click
        rua_logradouro.set "Turiaçu"
        numero_logradouro.set "123"
        calcular.click
        sleep 2
    end

    def retiradaLocal
        retirada.click
    end

    def preencherEnderecoRetirada
        sleep 1
        digitar_cep.set "09060320"
        sleep 2
        numero_logradouro.set "123"
        complemento.set "Teste Complemento"
        sleep 2
        avançarStep
    end

    def finalizarCompra
        sleep 1
        recaptcha.click
        recaptcha.click
        sleep 1
        botao_finalizar[1].click
    end

end