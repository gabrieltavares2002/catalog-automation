Dado('que acesso o catálogo dentro das catagorias') do
    sleep 1
  end
  
  Quando('adiciono os produtos ao carrinho') do
    produtosInicial.adicionaProdutos
  end
  
  Quando('vou para tela finalização de compra') do
    produtosInicial.fecharCarrinho
  end
  
  Quando('adiciono uma observação em um produto') do
    checkoutFinalizacao.adicionarObservacao
  end
  
  Quando('escolho CEP na entrega') do
    checkoutFinalizacao.calcularCep
  end
  
  Quando('preencho o cupom do tipo Porcentagem') do
    checkoutFinalizacao.cupomPorcentagem
  end
  
  Quando('escolho o tipo de pessoa Física') do
    checkoutFinalizacao.pessoaFisica
  end

  Quando('preencho o cupom do tipo Valor') do
    checkoutFinalizacao.cupomValor
  end
  
  Quando('escolho o tipo de pessoa Jurídica') do
    checkoutFinalizacao.pessoaJuridica
  end
  
  Quando('informo os dados do endereço um') do
    checkoutFinalizacao.preencherEnderecoCep
  end
  
  Quando('informo os dados do endereço dois') do
    checkoutFinalizacao.preencherEnderecoCep
  end
  
  Quando('informo os dados do endereço três') do
    checkoutFinalizacao.preencherEnderecoRetirada
  end
  
  Quando('escolho a forma de pagamento Online') do
    checkoutFinalizacao.preencherPagamentoOnline
    sleep 5
  end
  
  Quando('finalizo a compra') do
    checkoutFinalizacao.finalizarCompra
  end
  
  Então('todos os dados do pedido estão corretos') do
    expect(page).to have_content "Seu pedido foi finalizado com sucesso"
  end
  
  Quando('escolho LOGRADOURO na entrega') do
    checkoutFinalizacao.calcularLogradouro
  end
  
  Quando('escolho retirada') do
    checkoutFinalizacao.retiradaLocal
  end
  
  Quando('Adiciono uma observação no pedido') do
    checkoutFinalizacao.observacaoPedido
  end
  
  Quando('informo o troco') do
    checkoutFinalizacao.troco
  end
  
  Quando('escolho a forma de pagamento Dinheiro') do
    checkoutFinalizacao.pagamentoDinheiro
  end