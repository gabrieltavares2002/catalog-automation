#language:pt
# encoding: utf-8

@visitarCatalogo

Funcionalidade: Pedido no catálogo SmartPOS

    Contexto:
        Dado que acesso o catálogo dentro das catagorias

    @pedidoCepPagoOnline
    Cenário: Validar sucesso em pedido com entrega via CEP pago online
        Quando adiciono os produtos ao carrinho
        E vou para tela finalização de compra
        E adiciono uma observação em um produto
        E escolho CEP na entrega
        E preencho o cupom do tipo <tipo_cupom>
        E escolho o tipo de pessoa <tipo_pessoa>
        E informo os dados do endereço um
        E escolho a forma de pagamento Online
        E finalizo a compra
        Então todos os dados do pedido estão corretos
        Exemplos:
        |tipo_cupom     |tipo_pessoa  |
        |Porcentagem    |Física       |
        |Valor          |Física       |
        |Porcentagem    |Jurídica     |
        |Valor          |Jurídica     |

    @pedidoLogradouroPagoOnline
    Cenário: Validar sucesso em pedido com entrega via LOGRADOURO pago online
        Quando adiciono os produtos ao carrinho
        E vou para tela finalização de compra
        E adiciono uma observação em um produto
        E escolho LOGRADOURO na entrega
        E preencho o cupom do tipo <tipo_cupom>
        E escolho o tipo de pessoa <tipo_pessoa>
        E informo os dados do endereço dois
        E escolho a forma de pagamento Online
        E finalizo a compra
        Então todos os dados do pedido estão corretos
        Exemplos:
        |tipo_cupom     |tipo_pessoa  |
        |Porcentagem    |Física       |
        |Valor          |Física       |
        |Porcentagem    |Jurídica     |
        |Valor          |Jurídica     |
        

    @pedidoRetiradaPagoOnline
    Cenário: Validar sucesso em pedido com retirada pago Online
        Quando adiciono os produtos ao carrinho
        E vou para tela finalização de compra
        E adiciono uma observação em um produto
        E escolho retirada
        E preencho o cupom do tipo <tipo_cupom>
        E escolho o tipo de pessoa <tipo_pessoa>
        E informo os dados do endereço três
        E escolho a forma de pagamento Online
        E finalizo a compra
        Então todos os dados do pedido estão corretos
        Exemplos:
        |tipo_cupom     |tipo_pessoa  |
        |Porcentagem    |Física       |
        |Valor          |Física       |
        |Porcentagem    |Jurídica     |
        |Valor          |Jurídica     |

    @pedidoCepPresencial
    Cenário: Validar sucesso em pedido com entrega via CEP pago em Dinheiro com troco
        Quando adiciono os produtos ao carrinho
        E vou para tela finalização de compra
        E adiciono uma observação em um produto
        E escolho CEP na entrega
        E preencho o cupom do tipo <tipo_cupom>
        E escolho o tipo de pessoa <tipo_pessoa>
        E informo os dados do endereço um
        E escolho a forma de pagamento Dinheiro
        E Adiciono uma observação no pedido
        E finalizo a compra
        Então todos os dados do pedido estão corretos
        Exemplos:
        |tipo_cupom     |tipo_pessoa  |
        |Porcentagem    |Física       |
        |Valor          |Física       |
        |Porcentagem    |Jurídica     |
        |Valor          |Jurídica     |

    @pedidoLogradouroPresencial
    Cenário: Validar sucesso em pedido com entrega via pago em Dinheiro com troco
        Quando adiciono os produtos ao carrinho
        E vou para tela finalização de compra
        E adiciono uma observação em um produto
        E escolho LOGRADOURO na entrega
        E preencho o cupom do tipo <tipo_cupom>
        E escolho o tipo de pessoa <tipo_pessoa>
        E informo os dados do endereço dois
        E escolho a forma de pagamento Dinheiro
        E Adiciono uma observação no pedido
        E finalizo a compra
        Então todos os dados do pedido estão corretos
        Exemplos:
        |tipo_cupom     |tipo_pessoa  |
        |Porcentagem    |Física       |
        |Valor          |Física       |
        |Porcentagem    |Jurídica     |
        |Valor          |Jurídica     |
        

    @pedidoRetiradaPresencial
    Cenário: Validar sucesso em pedido com retirada pago em Dinheiro com troco
        Quando adiciono os produtos ao carrinho
        E vou para tela finalização de compra
        E adiciono uma observação em um produto
        E escolho retirada
        E preencho o cupom do tipo <tipo_cupom>
        E escolho o tipo de pessoa <tipo_pessoa>
        E informo os dados do endereço três
        E escolho a forma de pagamento Dinheiro
        E Adiciono uma observação no pedido
        E finalizo a compra
        Então todos os dados do pedido estão corretos
        Exemplos:
        |tipo_cupom     |tipo_pessoa  |
        |Porcentagem    |Física       |
        |Valor          |Física       |
        |Porcentagem    |Jurídica     |
        |Valor          |Jurídica     |





## CUPOM DE DESCONTO
## RETIRADA
## ENTREGA CEP
## ENTREGA LOGRADOURO
## FISICA
## JURIDICA
## PAG. PRESENCIAL
## PAG. ONLINE
## TROCO
## OBSERVAÇÃO PRODUTO
## OBSERVAÇÃO PEDIDO



