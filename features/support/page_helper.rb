Dir[File.join(File.dirname(__FILE__),'../pages/*_page.rb')].each { |file| require file }

# Modulo para chamar as classes instanciadas
module Pages
    
    def produtosInicial
        @produtosInicial ||= ProdutosInicial.new
    end

    def checkoutFinalizacao
        @checkoutFinalizacao ||= CheckoutFinalizacao.new
    end
end
