require "capybara"
require "capybara/cucumber"
require 'capybara/rspec'
require "selenium-webdriver"
require "site_prism"

require_relative "page_helper"
World(Pages)

#espera explicita
$wait = Selenium::WebDriver::Wait.new(timeout: 30)

#configuração Capybara
Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.default_max_wait_time = 30
end